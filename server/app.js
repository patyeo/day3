// this is to write a program similar to github io to set up the webpage in client
// web app are designed as stateless - meaning when request send & response receive, user is then disconnect
// from server. dont stay connected. data is pass through internet and once completed, connection ceased
// online games are stateful. where all players need to stay connected to server.

var express = require("express");
// to take in a 3rd party library

var app = express ();
// to construct an express and given the name 'app'

console.log(__dirname);
// to print out which directory i am on when i run this program

const NODE_PORT = process.env.PORT || 4000;
// to set the port, each program need a new port number
// in terminal, input export PORT = 4000
console.log(NODE_PORT);

app.use(express.static(__dirname + "/../client/"));
// to tell express where is my client file

app.get("/students", (req,res)=>{
// /students is the end point, defined by us, must make against the browser
// request (incoming) and response (outgoing) are callback function. when they match the end point.
    console.log("I am in students endpoint");
    var students = [
        {
            id: 1,
            name: "Kenneth",
            age: 25
        },
        {
            id: 2,
            name: "Alex",
            age: 26
        }
    ]
    res.status(200).json(students);
// the number is the http error code. this statement is needed whenever we need to return a response to
// client when they make a request
// http error code: 404-not found, 200-gd, 202-partially gd, 500-internal server error, usually express error
});

//need to handle request between client and server. need to define a few methods of protocol to answer the qn
// i can do something within express. wiki hypertext_transfer_protocol. impt is get, post, put
// get is to get back something, js, html, can be data (server serving you)
// post is to put something (client/browser send back to server, server receive & process), use to create a record
// put is use for update, eg update a record
// 2 parameters must be used for each of them
// restful API??

app.get("/students", (req,res)=>{
   console.log("I am in students endpoint");
   res.status(200).json(students);
});

app.get("/students/:student_id", (req,res)=>{
    console.log(req.params.student_id);
    let studentId = req.params.student_id;
    let studentObj = null;
    for(var x =0 ; x < students.length; x++){
        if(students[x].id == studentId){
            studentObj = students[x];
            break;
        }
    }
    res.status(200).json(studentObj);
}); 
var y = 5

// res.send can send anything, video, json, html
app.use((req,res,next)=>{
    y = y + 2;
    next();
}); 
app.use((req,res,next)=>{
    y = y + 3;
    next();
});
app.use((req,res,next)=>{
    var x = 6;
    try{
        console.log("Nothing found!");
        res.send(`<h1>Oops.. wrong place ${y}</h1>`);   
// use back tick when i want to use $ sign 
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    }
});    
// app.use can be use to initialize express library, it acts as a wild card when there is error in the request
//next is the internal end point, it will go on to activate the next app.use routine 
app.listen(NODE_PORT,()=>{
    console.log(`Web App started at ${NODE_PORT}`);
// or console.log ("Wed App started at " + NODE_PORT);
});

